---
## Why do I not see my passphrase diggits on the screen _ it's hidden _!!!

---

---
# . . . . . . . . . . . . . . . . .  _CHECK UPDATES ON YOUR SYSTEM REGULAR, FOR SECURITY !!!_
![Check_updates_parrot](/uploads/7bd73a070c810c112733575bb2b6deb5/Check_updates_parrot.png)
---

## STAP 1
# How to login encrypted System Linux ::: FIRST LOGIN

![luksWW](/uploads/3bceac4853ff1bcd994287a379b80e89/luksWW.png)



[First Login video ---> url:link  ...](https://gateway.pinata.cloud/ipfs/QmdGTe92HNe3rzYrrHJzVpNnukawenp278Remgk8gbajUr) 
-

- __NO  DIGITS ARE DISPLAYED - password is not shown - AND CURSOR IS NOT MOVING -__
- __it takes some time__ to process . patient .

---
---

## STAP 2
# How to change the USER password in the terminal (eg. xfce4-terminal) 

- The second __"t3st"__ password needs to be modified to a personal one. This one is weak now.
 
Steps to take ::::

- Open a terminal and
   copy/past the next word inside the terminal and hit ENTER 
   
   $ passwd
   
- Follow the questions

- Enter your current password

- And change password into a new one ...


----------------

[Change password video ---> url:link ...](https://gitlab.com/hubmus/raoni-login/-/blob/main/Change_password.gif) 
-
[Change password English version ---> url:link ...](https://gitlab.com/hubmus/raoni-login/-/blob/main/passwd.gif)
-


---

# Passwordmanager - 3 best - offline and online use

- [__BITWARDEN --> url:link__](https://bitwarden.com/) - _not installed_ -> need online account - very userfriendly - cheap - easy online - TOP 


- [__ENPASS --> url:link__](https://www.enpass.io/) - _installed on system_ -> need email reg: - userfriendly - cheap - offline/online - GOOD !!! 


- [__KEEPASSXC --> url:link__](https://keepassxc.org/) - _installed on system_ -> no reg: - popular - free - offline/online - Not friendly though widely used.



---


# VPN profider with unlimited devices. Good reviews. Price and service are good. 

## Surfshark

- [__SURFSHARK VPN ---> url:link__](https://surfshark.com/blog) - No gui for linux, yet. Easy to manage with networkmanager openvpn.

[_use surfshark without networkmanager gui._](https://gitlab.com/hubmus/openvpn-genmon-puppy)

